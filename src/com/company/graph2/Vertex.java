package com.company.graph2;

import java.util.ArrayList;

public class Vertex{
    private int index;
    private ArrayList<Vertex> inheritedVertexes;
    private ArrayList<Line> outgoingLines;

    public Vertex(int index) {
        this.index = index;
        inheritedVertexes = new ArrayList<>();
        outgoingLines = new ArrayList<>();
    }



    public int getIndex() {
        return index;
    }

    public ArrayList<Vertex> getInheritedVertexes() {
        return inheritedVertexes;
    }

    public void setInheritedVertexes(ArrayList<Vertex> inheritedVertexes) {
        this.inheritedVertexes = inheritedVertexes;
    }

    public ArrayList<Line> getOutgoingLines() {
        return outgoingLines;
    }


    public void setOutgoingLines(ArrayList<Line> outgoingLines) {
        this.outgoingLines = outgoingLines;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vertex vertex = (Vertex) o;
        return index == vertex.index;
    }

    @Override
    public int hashCode() {
        return Integer.hashCode(index);
    }

    @Override
    public String toString() {
        return "Vertex{" +
                "index=" + index +
                '}';
    }


}
