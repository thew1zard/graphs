package com.company.graph2;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import static java.util.Collections.sort;

public class GraphTasks2Solution implements GraphTasks2 {
    @Override
    public HashMap<Integer, Integer> dijkstraSearch(int[][] adjacencyMatrix, int startIndex) {

        HashMap<Integer, Integer> dijkstraResultMap = new HashMap<>();

        ArrayList<Vertex> vertexes = new ArrayList<>();
        ArrayDeque<Vertex> vertexesDeque = new ArrayDeque<>();
        HashSet<Vertex> resultList = new HashSet<>();
        ArrayList<Line> lineList = new ArrayList<>();
        for (int i = 0; i < adjacencyMatrix.length; i++) {
            vertexes.add(new Vertex(i));
        }


        //заполнение списка наследуемых вершин и списка ребер от вершины
        for (int i = 0; i < adjacencyMatrix.length; i++) {
            for (int j = 0; j < adjacencyMatrix[i].length; j++) {
                if (adjacencyMatrix[i][j] != 0) {
                    vertexes.get(i).getOutgoingLines().add(new Line(vertexes.get(i), vertexes.get(j), adjacencyMatrix[i][j]));

                }
            }

        }


        Vertex startVertex = vertexes.get(startIndex);
        vertexesDeque.offerFirst(startVertex);
        resultList.add(startVertex);
        dijkstraResultMap.put(startIndex, 0);


        int buffer = 0;
        while (resultList.size() != vertexes.size()) {

            //добавляем в список lineList ребра, которые идут от вершины в очереди
            for (int i = 0; i < vertexesDeque.getFirst().getOutgoingLines().size(); i++) {
                if (!resultList.contains(vertexesDeque.getFirst().getOutgoingLines().get(i).getSecondVertex())) {
                    Line someLine = vertexesDeque.getFirst().getOutgoingLines().get(i);
                    someLine.setValue(someLine.getValue() + buffer);
                    lineList.add(someLine);

                }
            }

            //находим ребро с минимальным весом
            Line lineWithMinimalWeight = lineList.get(0);
            for (int line = 0; line < lineList.size(); line++) {
                if (lineWithMinimalWeight.getValue() > lineList.get(line).getValue()) {
                    lineWithMinimalWeight = lineList.get(line);
                }
            }
            resultList.add(lineWithMinimalWeight.getSecondVertex());

            for (int i = 0; i < lineList.size(); i++) {
                if (resultList.contains(lineList.get(i).getSecondVertex()) && resultList.contains(lineList.get(i).getFirstVertex())) {
                    lineList.remove(i);
                    i--;
                }
            }

            for (int line = 0; line < lineList.size(); line++) {
                if (!lineList.get(line).equals(lineWithMinimalWeight)) {
                    lineList.get(line).setValue(lineList.get(line).getValue() - buffer);
                }
            }
            buffer = lineWithMinimalWeight.getValue();
            //добавляем в мапу это ребро
            dijkstraResultMap.put(lineWithMinimalWeight.getSecondVertex().getIndex(), lineWithMinimalWeight.getValue());
            //добавляем вершину в список вершин, для которых нашли минимальный путь
            vertexesDeque.offerLast(lineWithMinimalWeight.getSecondVertex());
            //удаляем из lineList это ребро
            lineList.remove(lineWithMinimalWeight);
            vertexesDeque.pollFirst();


        }

        return dijkstraResultMap;
    }

    @Override
    public Integer primaAlgorithm(int[][] adjacencyMatrix) {
        ArrayList<Vertex> vertexes = new ArrayList<>();
        ArrayDeque<Vertex> vertexesDeque = new ArrayDeque<>();
        HashSet<Vertex> resultList = new HashSet<>();
        ArrayList<Line> lineList = new ArrayList<>();
        for (int i = 0; i < adjacencyMatrix.length; i++) {
            vertexes.add(new Vertex(i));
        }


        //заполнение списка наследуемых вершин и списка ребер от вершины
        for (int i = 0; i < adjacencyMatrix.length; i++) {
            for (int j = 0; j < adjacencyMatrix[i].length; j++) {
                if (adjacencyMatrix[i][j] != 0) {
                    vertexes.get(i).getOutgoingLines().add(new Line(vertexes.get(i), vertexes.get(j), adjacencyMatrix[i][j]));

                }
            }

        }


        Vertex startVertex = vertexes.get(0);
        vertexesDeque.offerFirst(startVertex);
        resultList.add(startVertex);


        int buffer = 0;
        while (resultList.size() != vertexes.size()) {

            //добавляем в список lineList ребра, которые идут от вершины в очереди
            for (int i = 0; i < vertexesDeque.getFirst().getOutgoingLines().size(); i++) {
                if (!resultList.contains(vertexesDeque.getFirst().getOutgoingLines().get(i).getSecondVertex())) {
                    Line someLine = vertexesDeque.getFirst().getOutgoingLines().get(i);
                    someLine.setValue(someLine.getValue());
                    lineList.add(someLine);

                }
            }

            //находим ребро с минимальным весом
            Line lineWithMinimalWeight = lineList.get(0);
            for (int line = 0; line < lineList.size(); line++) {
                if (lineWithMinimalWeight.getValue() > lineList.get(line).getValue()) {
                    lineWithMinimalWeight = lineList.get(line);
                }
            }
            resultList.add(lineWithMinimalWeight.getSecondVertex());

            for (int i = 0; i < lineList.size(); i++) {
                if (resultList.contains(lineList.get(i).getSecondVertex()) && resultList.contains(lineList.get(i).getFirstVertex())) {
                    lineList.remove(i);
                    i--;
                }
            }


            buffer += lineWithMinimalWeight.getValue();
            //добавляем в мапу это ребро

            //добавляем вершину в список вершин, для которых нашли минимальный путь
            vertexesDeque.offerLast(lineWithMinimalWeight.getSecondVertex());
            //удаляем из lineList это ребро
            lineList.remove(lineWithMinimalWeight);
            vertexesDeque.pollFirst();


        }

        return buffer;
    }

    @Override
    public Integer kraskalAlgorithm(int[][] adjacencyMatrix) {

        ArrayList<Line> lineList = new ArrayList<>();
        ArrayList<Vertex> vertexes = new ArrayList<>();



        for (int i = 0; i < adjacencyMatrix.length; i++) {
            vertexes.add(new Vertex(i));
        }

        for (int i = 0; i < adjacencyMatrix.length; i++) {
            for (int j = 0; j < adjacencyMatrix[i].length; j++) {
                if (adjacencyMatrix[i][j] != 0 && adjacencyMatrix[j][i] != 0 && adjacencyMatrix[i][j] == adjacencyMatrix[j][i]) {
                    lineList.add(new Line(vertexes.get(i), vertexes.get(j), adjacencyMatrix[i][j]));
                    adjacencyMatrix[j][i] = 0;
                }
            }
        }
        sort(lineList);


        HashMap<HashSet<Vertex>, Integer> weightBySetWithVertex = new HashMap<>();
        int totalWeight = 0;


        for (Line line : lineList) {
            HashSet<Vertex> bufferSet = new HashSet<>();
            Vertex fV = line.getFirstVertex();
            Vertex sV = line.getSecondVertex();
            Integer weightOfSet = line.getValue();
            ArrayList<HashSet<Vertex>> components = new ArrayList<>();

            boolean isFound = false;


            bufferSet.add(fV);
            bufferSet.add(sV);

            if (weightBySetWithVertex.isEmpty()) {
                weightBySetWithVertex.put(bufferSet, line.getValue());
                totalWeight += line.getValue();
            } else {

                for (HashSet<Vertex> vertexSet : weightBySetWithVertex.keySet()) {
                    HashSet<Vertex> clonedBufferSet = (HashSet<Vertex>) bufferSet.clone();
                    HashSet<Vertex> clonedVertexSet = (HashSet<Vertex>) vertexSet.clone();
                    clonedBufferSet.retainAll(clonedVertexSet);
                    if (vertexSet.containsAll(bufferSet)) {
                        isFound = true;
                        break;
                    }
                    if (clonedBufferSet.size() != 0) {
                        components.add(vertexSet);
                        isFound = true;
                    }
                }
                if (!isFound) {
                    weightBySetWithVertex.put(bufferSet, weightOfSet);
                    totalWeight += weightOfSet;
                } else if (components.size() == 2) {
                    HashSet<Vertex> oldKey = components.get(0);
                    HashSet<Vertex> hashSet = new HashSet<>();
                    hashSet.addAll(components.get(0));
                    hashSet.addAll(components.get(1));
                    Integer weightForNewSet = weightBySetWithVertex.get(components.get(0)) + weightBySetWithVertex.get(components.get(1)) + weightOfSet;
                    weightBySetWithVertex.remove(oldKey);
                    weightBySetWithVertex.remove(components.get(1));
                    weightBySetWithVertex.put(hashSet, weightForNewSet);
                    totalWeight += weightOfSet;
                } else if (components.size() == 1) {
                    HashSet<Vertex> oldKey = components.get(0);
                    Integer weightForNewKey = weightBySetWithVertex.get(components.get(0)) + weightOfSet;
                    weightBySetWithVertex.remove(oldKey);
                    oldKey.addAll(bufferSet);
                    weightBySetWithVertex.put(oldKey, weightForNewKey);
                    totalWeight += weightOfSet;
                }
            }


        }

        return totalWeight;
    }
}
