package com.company.graph2;

import java.util.Objects;

public class Line implements Comparable{

    private Vertex firstVertex;
    private Vertex secondVertex;

    private Integer value;

    public Line(Vertex fV, Vertex sV, int value){
        this.firstVertex = fV;
        this.secondVertex = sV;
        this.value = value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public Vertex getSecondVertex() {
        return secondVertex;
    }

    public int getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Line line = (Line) o;
        return value == line.value &&
                Objects.equals(firstVertex, line.firstVertex) &&
                Objects.equals(secondVertex, line.secondVertex);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstVertex, secondVertex, value);
    }

    public Vertex getFirstVertex() {
        return firstVertex;
    }

    @Override
    public String toString() {
        return "Line{" +
                "firstVertex=" + firstVertex +
                ", secondVertex=" + secondVertex +
                ", value=" + value +
                '}';
    }

    @Override
    public int compareTo(Object o) {
        return this.value.compareTo(((Line) o).getValue());
    }
}
